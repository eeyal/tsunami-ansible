# README #
########################################################################
Ansible playbooks project to install tsunami on multiple hosts
run the scan and then run the playbook to check the results and in case 
the scan havent succedded - will notify to slack channel
########################################################################
content:
playbooks: 
install-and-run-tsunami.yml: install java google tsunami network scanner,  and then fire a scan run on given hosts.
check-result: check tsunami run output file and in case the scan haven't succedded - will notify on given slack channel.
Role: Java - this role is in use in the install cookbook inorder to install java on the machines.
Vars: default.yml - configuration file.
### How do I get set up? ###

prerequisite: in order to use this solution which is base on Ansible, you need to have one control node with ssh access to your serves inventory.

How to use:
1. update the servers you wish to scan in the servers_list variable, your slack channel and token - to notify in case of a not succedded run.
2. run the playbook: install-and-run-tsunami.yml.
3. after the scan finished, run the  playbook check-result.yml.